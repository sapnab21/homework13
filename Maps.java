import java.util.*;
public class Maps {
    public static void main(String[] args) {

        HashMap<Integer, String> streetNumberandName = new HashMap<>();
        streetNumberandName.put(1000, "Liam");
        streetNumberandName.put(1001, "Noah");
        streetNumberandName.put(1002, "Olivia");
        streetNumberandName.put(1003, "Emma");
        streetNumberandName.put(1004, "Benjamin");
        streetNumberandName.put(1005, "Evelyn");
        streetNumberandName.put(1006, "Lucas");

        System.out.println("The person who lives on 1004 is: " + streetNumberandName.get(1004));
        System.out.println("The odd number streets and people who live on them are: ");
        for (int i=1000; i<=1006; i++){
            if (i%2 != 0) {
                System.out.println(i + " " + streetNumberandName.get(i));
            }
        }

    }
}
